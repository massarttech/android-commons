package com.massarttech.android.common.helper;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class CommonUtilsTest {

    @Test
    public void isNetworkAvailable() {
        Context context = InstrumentationRegistry.getInstrumentation()
                .getContext();
        assertTrue(CommonUtils.isNetworkAvailable(context));
    }

    @Test
    public void whenIsValidEmailSuccess() {
        assertTrue(CommonUtils.isValidEmail("test@cafebabe.com"));
    }

    @Test
    public void whenIsValidEmailFailure() {
        assertTrue(CommonUtils.isValidEmail("test@cafebabe."));
    }

}