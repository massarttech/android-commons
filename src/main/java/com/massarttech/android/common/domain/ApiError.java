package com.massarttech.android.common.domain;


import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.massarttech.android.common.R;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@Deprecated
@SuppressWarnings("WeakerAccess")
public class ApiError implements Serializable {
    private       String  title;
    private final int     status;
    private final long    timestamp;
    private       String  message;
    private final boolean resolvable;

    public AlertDialog.Builder toDialog(@NonNull Context context,
                                        @Nullable String defaultMessage,
                                        @Nullable String defaultTitle) {
        return new AlertDialog.Builder(context)
                .setMessage(message == null ? defaultMessage : message)
                .setTitle(title == null ? defaultTitle : title);
    }

    public AlertDialog.Builder toDialog(@NonNull Context context) {
        return toDialog(context, context.getString(R.string.unknown_error), null);
    }

    public static ApiError fromThrowable(@NonNull Throwable throwable) {
        return new ApiError("", 500, System.currentTimeMillis(),
                throwable.getLocalizedMessage(),
                false);
    }


}
