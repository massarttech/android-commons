package com.massarttech.android.common.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Patterns;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import androidx.annotation.StringRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.massarttech.android.common.helper.UiUtils.loadBitmapFromView;

@SuppressWarnings("unused,WeakerAccess")
public final class CommonUtils {
    @Nullable
    public static Bitmap blur(Context context, View view, float radius, float scale) {
        Bitmap image = loadBitmapFromView(view);
        if (image == null) {
            return null;
        }
        int width  = Math.round(image.getWidth() * scale);
        int height = Math.round(image.getHeight() * scale);

        Bitmap inputBitmap  = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript        rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic;
        theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn  = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(radius);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);


        return outputBitmap;
    }

    public static String getDurationBreakdown(long millis) {
        if (millis < 0) {
            throw new IllegalArgumentException("Duration must be greater than zero!");
        }

        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        StringBuilder sb = new StringBuilder(64);
        if (days > 0) {
            sb.append(days).append(days == 1 ? " Day & " : " Days & ");
        }
        sb.append(hours > 9 ? hours : "0" + hours);
        sb.append(":");
        sb.append(minutes > 9 ? minutes : "0" + minutes);
        sb.append(":");
        sb.append(seconds > 9 ? seconds : "0" + seconds);
        return (sb.toString());
    }

    private CommonUtils() {
        throw new IllegalStateException("Nah! 0xcafebabe");
    }

    @RequiresPermission(Manifest.permission.ACCESS_NETWORK_STATE)
    public static boolean isNetworkAvailable(@NonNull Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    public static void askPermission(@NonNull Activity context,
                                     int requestCode,
                                     @NonNull String permission,
                                     String... morePermissions) {
        if (morePermissions.length > 0) {
            ActivityCompat.requestPermissions(context,
                    morePermissions, requestCode);
        } else {

            ActivityCompat.requestPermissions(context,
                    new String[]{permission}, requestCode);
        }

    }

    public static boolean isPermission(@NonNull Context context, @NonNull String permission) {
        return ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isValidEmail(@Nullable CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    /**
     * Helper method which should be used with {@link androidx.recyclerview.widget.GridLayoutManager}
     * to set the proper count of item per row
     *
     * @param itemWidthDp Width of each item like 180,100. This width is in DPs.
     * @param context     Valid context
     * @return number of item that should be displayed in each row.
     */
    public static int getSpanCountByItemWidth(final int itemWidthDp, @NonNull final Context context) {
        int width = context.getResources().getDisplayMetrics().widthPixels;
        return (int) (width / pxFromDp(context, itemWidthDp));
    }

    public static float dpFromPx(@NonNull final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }

    public static float pxFromDp(@NonNull final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

    public static boolean isRtl(@NonNull Context context) {
        return context.getResources().getConfiguration().getLayoutDirection() ==
                View.LAYOUT_DIRECTION_RTL;
    }

    public static String americanDateFormat(long millis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return dateFormat.format(millis);
    }

    public static boolean isValidDiff(Calendar current, Calendar select, int year) {
        int yearsPassed = current.get(Calendar.YEAR) - select.get(Calendar.YEAR);
        if (yearsPassed > year) {
            return true;
        }
        int daysPassed  = current.get(Calendar.DAY_OF_MONTH) - select.get(Calendar.DAY_OF_MONTH);
        int monthPassed = current.get(Calendar.MONTH) - select.get(Calendar.MONTH);

        if (yearsPassed == year) {
            if (monthPassed > 0) {
                return true;
            }
            return monthPassed == 0 && daysPassed >= 0;
        }
        return false;
    }

    @Deprecated
    /*
     * Use getSpanCountByItemWidth
     */
    public static int calculateNoOfColumns(Context context, int width) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float          dpWidth        = displayMetrics.widthPixels / displayMetrics.density;
        return (int) (dpWidth / width);
    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     * <p>
     * lat1, lon1 Start point lat2, lon2  End point
     *
     * @return Distance in Meters
     */
    public static double distanceInMeters(double lat1, double lat2, double lon1,
                                          double lon2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c        = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters


        distance = Math.pow(distance, 2);

        return Math.sqrt(distance);
    }

    public static double distanceInKm(double lat1, double lat2, double lon1,
                                      double lon2) {
        return distanceInMeters(lat1, lat2, lon1, lon2) / 1000.0;
    }

    /**
     * Format the given timestamp.<p> If time less than one day than just return
     * hours and minutes 10:20 AM otherwise return with date 02 May - 12:11 PM
     * </p>
     *
     * @param timestamp Time to format
     * @return formatted time
     */
    public static String getFormattedTime(long timestamp) {

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24  60  60 * 1000;

        long timeDifference = System.currentTimeMillis() - timestamp;

        return timeDifference < oneDayInMillis
                ? DateFormat.format("hh:mm a", timestamp).toString()
                : DateFormat.format("dd MMM - hh:mm a", timestamp).toString();
    }

    /**
     * Time duration from now.
     *
     * @param time   Time
     * @param inPast Whether the given time is in past
     * @return String formatted time 12:12:12
     */
 /*   public static String durationFromNow(long time, boolean inPast) {
        if (inPast) {
            return formatDuration(System.currentTimeMillis() - time);
        } else {
            return formatDuration(System.currentTimeMillis() - time);
        }
    }*/

    /**
     * Format give milliseconds in duration. THIS METHOD DOES NOT CALCULATE TIME DIFFERENCE
     *
     * @param duration in milliseconds
     * @return 00:11:22 in this format
     */
    public static String formatDuration(long duration) {
        long hours   = TimeUnit.MILLISECONDS.toHours(duration);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) % 60;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(duration) % 60;
        if (hours < 1) {
            return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
        }
        return String.format(Locale.getDefault(), "%02d:%02d:%02d", hours, minutes, seconds);
    }

    /**
     * Format given time like 20 seconds,10 minutes,10 days,10 months,10 years
     *
     * @param millis given time
     * @return format string
     */
    public static String convertTimeToString(long millis) {
        millis = System.currentTimeMillis() - millis;
        long                  seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
        long                  minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        long                  hours   = TimeUnit.MILLISECONDS.toHours(millis);
        long                  days    = TimeUnit.MILLISECONDS.toDays(millis);
        long                  months  = (long) (days / 30.4167);
        long                  years   = months / 12;
        TranslationHelper.Key key;
        long                  time;
        if (seconds < 60) {
            time = seconds;
            key  = seconds > 1 ? TranslationHelper.Key.SECONDS : TranslationHelper.Key.SECOND;
        } else if (minutes < 60) {
            time = minutes;
            key  = minutes > 1 ? TranslationHelper.Key.MINUTES : TranslationHelper.Key.MINUTE;
        } else if (hours < 24) {
            time = hours;
            key  = hours > 1 ? TranslationHelper.Key.HOURS : TranslationHelper.Key.HOUR;
        } else if (days < 31) {
            time = days;
            key  = days > 1 ? TranslationHelper.Key.DAYS : TranslationHelper.Key.DAY;
        } else if (months < 12) {
            time = months;
            key  = months > 1 ? TranslationHelper.Key.MONTHS : TranslationHelper.Key.MONTH;
        } else {
            time = years;
            key  = years > 1 ? TranslationHelper.Key.YEARS : TranslationHelper.Key.YEAR;
        }
        return String.format(Locale.getDefault(), "%d %s", time,
                TranslationHelper.getInstance().getTranslation(key, Locale.getDefault()));

    }

    /**
     * Get street address of a location
     *
     * @param context Can be any context but better to use Application Context
     * @param lat     latitude
     * @param lon     longitude
     * @return null if address not found other street address of given location
     */
    @Nullable
    public static String getAddress(@NonNull Context context, double lat, double lon) {
        List<Address> addresses = null;
        Geocoder      geocoder  = new Geocoder(context, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lat, lon, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses == null || addresses.size() == 0) {
            return null;
        }
        Address address = addresses.get(0);
        if (address.getMaxAddressLineIndex() < 0) {
            return null;
        }
        return address.getAddressLine(0);
    }

    /**
     * Format the given number in Kilo, Million or Billion
     *
     * @param number number to format
     * @return String with two decimal point accuracy
     * with K,M or B at end e.g 2.3M
     */
    public static String formatNumber(int number) {
        if (number < 1000) {
            return String.valueOf(number);
        } else if (number < 1000000) {
            return String.format(Locale.getDefault(), "%.2f K", (float) number / 1000f);
        } else if (number < 1000000000) {
            return String.format(Locale.getDefault(), "%.2f M", (float) number / 1000000f);
        } else {
            return String.format(Locale.getDefault(), "%.2f B", (float) number / 1000000000f);
        }
    }

    public static Spanned htmlString(@NonNull Context context, @StringRes int id, Object... args) {
        String html = context.getString(id, args);
        return Html.fromHtml(html);
    }

    @NonNull
    public static String generateId() {
        return "id" + System.currentTimeMillis();
    }

    public static Spanned toHtml(@NonNull String message) {
        return Html.fromHtml(message);
    }


}
