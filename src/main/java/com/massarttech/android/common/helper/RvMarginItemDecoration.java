package com.massarttech.android.common.helper;

import android.graphics.Rect;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import lombok.Builder;

@Builder
@SuppressWarnings("unused,WeakerAccess")
public class RvMarginItemDecoration extends RecyclerView.ItemDecoration {
    private final int marginTop;
    private final int marginLeft;
    private final int marginRight;
    private final int marginBottom;

    public RvMarginItemDecoration(int marginTop, int marginLeft, int marginRight, int marginBottom) {
        this.marginTop = marginTop;
        this.marginLeft = marginLeft;
        this.marginRight = marginRight;
        this.marginBottom = marginBottom;
    }

    public RvMarginItemDecoration(int margin) {
        this(margin, margin, margin, margin);
    }


    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = marginBottom;
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = marginTop;
        }
        outRect.right = marginRight;
        outRect.left = marginLeft;
    }
}
