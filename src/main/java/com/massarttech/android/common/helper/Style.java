package com.massarttech.android.common.helper;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

import androidx.annotation.NonNull;

import com.massarttech.android.common.R;

import lombok.Getter;

@Getter
public class Style {
    private final int secondaryColor;
    private final int primaryColor;
    private final int primaryDarkColor;
    private final int windowBackground;

    @SuppressLint("ResourceType")
    public Style(@NonNull Context context) {
        TypedValue      typedValue = new TypedValue();
        Resources.Theme theme      = context.getTheme();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        primaryColor = typedValue.data;
        theme.resolveAttribute(R.attr.colorSecondary, typedValue, true);
        secondaryColor = typedValue.data;
        theme.resolveAttribute(R.attr.colorPrimaryDark, typedValue, true);
        primaryDarkColor = typedValue.data;
        theme.resolveAttribute(android.R.attr.windowBackground, typedValue, true);
        windowBackground = typedValue.data;
    }
}