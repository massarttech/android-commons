package com.massarttech.android.common.helper;

import android.app.Activity;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.util.Consumer;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.massarttech.android.common.R;


public final class DialogUtil {
    private DialogUtil() {

    }

    public static void showOk(@NonNull Activity activity,
                              @Nullable CharSequence title,
                              @Nullable CharSequence message,
                              @Nullable Consumer<DialogInterface> onDismiss) {
        new MaterialAlertDialogBuilder(activity)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok, null)
                .setOnDismissListener(dialog -> {
                    if (onDismiss != null) {
                        onDismiss.accept(dialog);
                    }
                })
                .show();
    }

    public static void showOk(@NonNull Activity activity,
                              @StringRes int title,
                              @StringRes int message,
                              Object... messageArgs) {
        showOk(activity, CommonUtils.htmlString(activity, title), CommonUtils.htmlString(activity, message, messageArgs), null);
    }

    public static void showOk(@NonNull Activity activity,
                              @StringRes int title,
                              @StringRes int message,
                              @NonNull Consumer<DialogInterface> onDone,
                              Object... messageArgs) {
        showOk(activity, CommonUtils.htmlString(activity, title), CommonUtils.htmlString(activity, message, messageArgs), onDone);
    }

    public static void showOk(@NonNull Activity activity,
                              @Nullable String message) {
        if (message != null) {
            showOk(activity, null, CommonUtils.toHtml(message), null);
        }
    }

    public static void showOk(@NonNull Activity activity,
                              @Nullable String message,
                              @NonNull Consumer<DialogInterface> onDismiss) {
        if (message != null) {
            showOk(activity, null, CommonUtils.toHtml(message), onDismiss);
        }
    }

    public static void showOk(@NonNull Activity activity,
                              @StringRes int message) {
        showOk(activity, null, CommonUtils.htmlString(activity, message), null);
    }

    public static void showOk(@NonNull Activity activity,
                              @StringRes int message,
                              @NonNull Consumer<DialogInterface> onDone) {
        showOk(activity, null, CommonUtils.htmlString(activity, message), onDone);
    }

    public static void showOk(@NonNull Activity activity, @NonNull String title, @NonNull String message) {
        showOk(activity, title, message, null);
    }
}
