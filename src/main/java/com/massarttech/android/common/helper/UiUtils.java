package com.massarttech.android.common.helper;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Calendar;

import java8.util.function.BiConsumer;
import java8.util.function.Consumer;

/**
 * Common UI utilities
 */
@SuppressWarnings("unused,WeakerAccess")
public final class UiUtils {
    private UiUtils() {
        throw new IllegalStateException("Nah! 0xcafebabe");
    }
    /**
     * Blur given view. It creates bitmap of view
     * that should be set explicitly to {@link android.widget.ImageView}
     * If view width or height is 0 than it will throw {@link IllegalArgumentException}
     *
     * @param context Context of the view
     * @param view    View to blur
     * @param radius  blur radius
     * @param scale   view scaling while bullring
     * @return Blur bitmap
     */
    @Nullable
    public static Bitmap blur(@NonNull Context context,
                              @NonNull View view,
                              @FloatRange(from = 0.1) float radius,
                              @FloatRange(from = 0.1) float scale) {
        Bitmap image = loadBitmapFromView(view);
        if (image == null) {
            return null;
        }
        return blur(context, image, radius, scale);

    }

    /**
     * Blur given view. It creates bitmap of view
     * that should be set explicitly to {@link android.widget.ImageView}
     * If view width or height is 0 than it will throw {@link IllegalArgumentException}
     *
     * @param context Context of the view
     * @param image   Bitmap to blur
     * @param radius  blur radius
     * @param scale   view scaling while bullring
     * @return Blur bitmap
     */
    public static Bitmap blur(@NonNull Context context,
                              @NonNull Bitmap image,
                              @FloatRange(from = 0.1) float radius,
                              @FloatRange(from = 0.1) float scale) {
        int width  = Math.round(image.getWidth() * scale);
        int height = Math.round(image.getHeight() * scale);

        Bitmap inputBitmap  = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript        rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic;
        theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn  = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(radius);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);


        return outputBitmap;
    }

    /**
     * Create bitmap of a view
     *
     * @param view view to create bitmap of. Width and height must of > 0
     * @return View's bitmap
     */
    @Nullable
    public static Bitmap loadBitmapFromView(@NonNull View view) {
        int width  = view.getWidth();
        int height = view.getHeight();
        if (height < 1 || width < 1) {
            return null;
        }
        int measuredWidth  = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);

        view.measure(measuredWidth, measuredHeight);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);

        view.draw(c);

        return b;
    }


    /**
     * Show Date picker dialog.
     *
     * @param context     Valid context
     * @param minDate     Minimum date that can be selected
     * @param maxDate     Maximum date that can be selected
     * @param currentDate Current date which should be selected
     * @param consumer    Listener when date is picker
     * @return DataPickerDialog in case of needed anywhere else
     */
    public static DatePickerDialog showDatePicker(@NonNull Context context,
                                                  @NonNull Calendar minDate,
                                                  @NonNull Calendar maxDate,
                                                  @NonNull Calendar currentDate,
                                                  @NonNull BiConsumer<Calendar, String> consumer) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, (view, year, month, dayOfMonth) -> {
            Calendar toReturn = Calendar.getInstance();
            toReturn.set(year, month, dayOfMonth);
            consumer.accept(toReturn, CommonUtils.americanDateFormat(toReturn.getTimeInMillis()));
        }, currentDate.get(Calendar.YEAR), currentDate.get(Calendar.MONTH),
                currentDate.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.getDatePicker().setMinDate(minDate.getTimeInMillis());
        datePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
        datePickerDialog.show();
        return datePickerDialog;
    }

    /**
     * Make toolbar gradient
     *
     * @param toolbar  Toolbar
     * @param gradient Gradient bg resource
     * @param color    Color which will be applied to toolbar text
     */
    public static void makeGradientToolbar(@NonNull Toolbar toolbar, @DrawableRes int gradient, @ColorRes int color) {
        toolbar.setBackground(toolbar.getContext().getDrawable(gradient));
        toolbar.setTitleTextColor(ContextCompat.getColor(toolbar.getContext(), android.R.color.white));
        if (toolbar.getNavigationIcon() != null) {
            toolbar.getNavigationIcon()
                    .setTint(ContextCompat.getColor(toolbar.getContext(), android.R.color.white));
        }

    }

    /**
     * Hide the soft keyboard giving activity on which it is visible
     *
     * @param activity Activity on which keyboard is visible
     */
    public static void hideKeyboard(@NonNull Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    /**
     * Validates edit text
     *
     * @param editText    Edit text which is being validated. I show error or clear error on it
     * @param value       Value of the edit text
     * @param error       @StringRes of error
     * @param validate    ValidationClause
     * @param onValidated can be null, do anything when field is valid
     * @return true/false if valid/invalid
     * e.g
     * <code>
     * validateField(etName, name,
     * R.string.input_valid_name,
     * value -> value.length() > 3,
     * customer::setName)
     * </code>
     */
    public static boolean validateField(@NonNull TextInputEditText editText,
                                        @NonNull CharSequence value,
                                        @StringRes int error,
                                        @NonNull ValidateClause validate,
                                        @Nullable Consumer<String> onValidated) {
        if (validate.validate(value)) {
            editText.setError(null);
            if (onValidated != null) {
                onValidated.accept(value.toString());
            }
            return true;
        } else {
            editText.setError(editText.getContext().getString(error));
            return false;
        }
    }

    /**
     * Used to provide validation for validateField method
     * Use it as lambda
     */
    @FunctionalInterface
    public interface ValidateClause {
        boolean validate(CharSequence value);
    }
}
