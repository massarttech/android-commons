package com.massarttech.android.common.helper;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;

@SuppressWarnings("unused")
public final class CommonIntents {
    private CommonIntents() {
        throw new IllegalStateException("Nah! 0xcafebabe");
    }

    public static void makeCall(@NonNull Context context, @NonNull String number, boolean addPlus) {
        String prefix = addPlus ? "tel+:" : "tel:";
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(prefix + number));
        context.startActivity(intent);

    }

    public static void openInPlayStore(@NonNull Context context, @NonNull String appId) {
        Uri    uri        = Uri.parse("market://details?id=" + appId);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + appId)));
        }
    }
}
