package com.massarttech.android.common.helper;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Locale;

public final class TranslationHelper {
    private static final TranslationHelper instance;

    static {
        instance = new TranslationHelper();
    }

    private final HashMap<String, HashMap<Key, String>> translationsMap;

    private TranslationHelper() {
        translationsMap = new HashMap<>();
        fillEnglish();
        fillArabic();
        fillTurkish();
    }

    public static TranslationHelper getInstance() {
        return instance;
    }

    private void fillArabic() {
        HashMap<Key, String> arabicMap = new HashMap<>();
        translationsMap.put("ar", arabicMap);
        arabicMap.put(Key.SECOND, "ثانية");
        arabicMap.put(Key.SECONDS, "ثواني");
        arabicMap.put(Key.MINUTE, "دقيقة");
        arabicMap.put(Key.MINUTES, "دقائق");
        arabicMap.put(Key.HOUR, "ساعة");
        arabicMap.put(Key.HOURS, "ساعات");
        arabicMap.put(Key.DAY, "يوم");
        arabicMap.put(Key.DAYS, "أيّام");
        arabicMap.put(Key.MONTH, "شهر");
        arabicMap.put(Key.MONTHS, "شهور");
        arabicMap.put(Key.YEAR, "سنة");
        arabicMap.put(Key.YEARS, "سنوات");
    }

    private void fillTurkish() {
        HashMap<Key, String> tr = new HashMap<>();
        translationsMap.put("tr", tr);
        tr.put(Key.SECOND, "Saniye");
        tr.put(Key.SECONDS, "Saniye");
        tr.put(Key.MINUTE, "Dakika");
        tr.put(Key.MINUTES, "Dakika");
        tr.put(Key.HOUR, "Saat");
        tr.put(Key.HOURS, "Saat");
        tr.put(Key.DAY, "Gün");
        tr.put(Key.DAYS, "Gün");
        tr.put(Key.MONTH, "Ay");
        tr.put(Key.MONTHS, "Ay");
        tr.put(Key.YEAR, "Yıl");
        tr.put(Key.YEARS, "Yıl");
    }

    @NonNull
    public String getTranslation(@NonNull Key key, @NonNull Locale locale) {
        HashMap<Key, String> translations = translationsMap.get(locale.getLanguage());
        if (translations == null) {
            return "";
        }
        String translation = translations.get(key);
        if (translation == null) {
            return "";
        }
        return translation;
    }

    private void fillEnglish() {
        HashMap<Key, String> englishMap = new HashMap<>();
        translationsMap.put(Locale.ENGLISH.getLanguage(), englishMap);
        englishMap.put(Key.SECOND, "Second");
        englishMap.put(Key.SECONDS, "Seconds");
        englishMap.put(Key.MINUTE, "Minute");
        englishMap.put(Key.MINUTES, "Minutes");
        englishMap.put(Key.HOUR, "Hour");
        englishMap.put(Key.HOURS, "Hours");
        englishMap.put(Key.DAY, "Day");
        englishMap.put(Key.DAYS, "Days");
        englishMap.put(Key.MONTH, "Month");
        englishMap.put(Key.MONTHS, "Months");
        englishMap.put(Key.YEAR, "Year");
        englishMap.put(Key.YEARS, "Years");
    }

    public enum Key {
        SECOND, SECONDS, MINUTE, HOUR, HOURS, MINUTES, DAY, DAYS, MONTH, MONTHS, YEAR, YEARS
    }
}
