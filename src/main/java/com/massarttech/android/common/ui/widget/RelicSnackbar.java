package com.massarttech.android.common.ui.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.snackbar.SnackbarContentLayout;
import com.massarttech.android.common.R;
import com.massarttech.android.common.helper.CommonUtils;

/**
 * Helper class to make snackbar look like shown in Relic interface
 * Usage is same is Snackbar. It is required to set gradient text in your app main
 * class file.
 * e.g <code>{@link RelicSnackbar#generateBg(int, int)}</code>
 * <pre>
 *     RelicSnackbar.make(view, "message",R.drawable.ic_info)
 *             .setAction("Click",v->System.out.println("Clicked"))
 *             .show()
 * </pre>
 */
@SuppressWarnings("unused,WeakerAccess")
public final class RelicSnackbar {
    private static int actionTextColor = Color.WHITE;
    private static GradientDrawable bgGradient;

    static {
        generateBg(Color.parseColor("#0570b5"),
                Color.parseColor("#84bc39"),
                actionTextColor,
                GradientDrawable.Orientation.LEFT_RIGHT);
    }

    private final Snackbar snackbar;

    private RelicSnackbar(@NonNull Snackbar snackbar) {
        this.snackbar = snackbar;
    }

    /**
     * Call this method to generate gradient background.
     * It's better to call this method in your App starting point. It will use the same
     * background throughout the app. The orientation of gradient will be
     * {@link GradientDrawable.Orientation#LEFT_RIGHT} i.e left to right
     *
     * @param startColor gradient start color
     * @param endColor   gradient end color
     */
    public static void generateBg(int startColor, int endColor) {
        generateBg(startColor, endColor, GradientDrawable.Orientation.LEFT_RIGHT);
    }

    /**
     * Call this method to generate gradient background.
     * It's better to call this method in your App starting point. It will use the same
     * background throughout the app.
     *
     * @param startColor  gradient start color
     * @param endColor    gradient end color
     * @param orientation orientation of gradient. {@link GradientDrawable.Orientation}
     */
    public static void generateBg(int startColor, int endColor,
                                  @NonNull GradientDrawable.Orientation orientation) {
        generateBg(startColor, endColor, actionTextColor,
                GradientDrawable.Orientation.LEFT_RIGHT);
    }

    /**
     * Call this method to generate gradient background.
     * It's better to call this method in your App starting point. It will use the same
     * background throughout the app.
     *
     * @param startColor      gradient start color
     * @param endColor        gradient end color
     * @param actionTextColor Action text color
     * @param orientation     orientation of gradient. {@link GradientDrawable.Orientation}
     */
    public static void generateBg(int startColor, int endColor,
                                  int actionTextColor,
                                  @NonNull GradientDrawable.Orientation orientation) {
        bgGradient = new GradientDrawable(orientation,
                new int[]{startColor, endColor});
        RelicSnackbar.actionTextColor = actionTextColor;
        bgGradient.setCornerRadius(20f);
    }

    /**
     * Call this method to generate gradient background.
     * It's better to call this method in your App starting point. It will use the same
     * background throughout the app.
     *
     * @param startColor      gradient start color
     * @param endColor        gradient end color
     * @param actionTextColor Action text color
     */
    public static void generateBg(int startColor, int endColor,
                                  int actionTextColor) {
        RelicSnackbar.actionTextColor = actionTextColor;
        generateBg(startColor, endColor, actionTextColor,
                GradientDrawable.Orientation.LEFT_RIGHT);
    }

    public static RelicSnackbar make(@NonNull View view, @NonNull String message,
                                     @DrawableRes int icon,
                                     Duration duration) {
        Context                 context     = view.getContext();
        int                     orientation = context.getResources().getConfiguration().orientation;
        Snackbar                snackbar    = Snackbar.make(view, message, duration.value);
        Snackbar.SnackbarLayout layout      = (Snackbar.SnackbarLayout) snackbar.getView();
        for (int i = 0; i < layout.getChildCount(); i++) {
            View view1 = layout.getChildAt(i);
            if (view1 instanceof SnackbarContentLayout) {
                SnackbarContentLayout contentLayout = (SnackbarContentLayout) view1;
                contentLayout.setBackground(bgGradient);
                ViewGroup.LayoutParams layoutParams = contentLayout.getLayoutParams();
                if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) layoutParams;
                    params.bottomMargin = 40;
                }
                contentLayout.setLayoutParams(layoutParams);
                break;
            }
        }
        TextView textView = layout.findViewById(R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        if (icon != 0) {
            textView.setCompoundDrawablePadding(10);
            if (CommonUtils.isRtl(context)) {
                textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, icon, 0);
            } else {
                textView.setCompoundDrawablesWithIntrinsicBounds(icon, 0, 0, 0);
            }
            textView.setCompoundDrawablePadding(((int) CommonUtils.pxFromDp(textView.getContext(), 10)));
        }
        layout.setBackgroundColor(Color.TRANSPARENT);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        snackbar.setActionTextColor(actionTextColor);
        return new RelicSnackbar(snackbar);
    }


    public static RelicSnackbar make(@NonNull View view, @NonNull String message) {
        return make(view, message, 0, Duration.LONG);
    }

    public static RelicSnackbar make(@NonNull View view, @NonNull String message, @NonNull Duration duration) {
        return make(view, message, 0, duration);
    }

    public static RelicSnackbar make(@NonNull View view, @NonNull String message, @DrawableRes int icon) {
        return make(view, message, icon, Duration.LONG);
    }

    public void show() {
        snackbar.show();
    }

    public RelicSnackbar setActionTextColor(int color) {
        actionTextColor = color;
        snackbar.setActionTextColor(color);
        return this;
    }

    public RelicSnackbar setAction(@NonNull String title,
                                   @NonNull View.OnClickListener clickListener) {
        snackbar.setAction(title, clickListener);
        return this;
    }

    public void dismiss() {
        snackbar.dismiss();
    }

    public boolean isShown() {
        return snackbar.isShown();
    }

    public void setText(@NonNull String text) {
        snackbar.setText(text);
    }

    public enum Duration {
        INDEFINITE(-2), LONG(-1), SHORT(0);
        private final int value;

        Duration(int value) {
            this.value = value;
        }

    }
}
