package com.massarttech.android.common.ui.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import com.massarttech.android.common.BuildConfig;

/**
 * Draw curved shadow effect
 */
public class RelicEffect extends View {
    private static final int   WIDTH_FACTOR = 80;
    private static final float STROKE       = 0.5f;
    private              Paint mPaint;
    private              RectF mRect;

    public RelicEffect(Context context) {
        super(context);
    }

    public RelicEffect(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RelicEffect(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec + WIDTH_FACTOR, heightMeasureSpec);
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mPaint = null;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mPaint == null) {
            initializeDrawing();
        }
        long time = System.currentTimeMillis();
        for (int i = 0; i < 60; i++) {
            mRect.bottom = getBottom() - (i + 1) * STROKE;
            mRect.top = getTop() - i * STROKE;
            int colorChange = (int) (i * 1.5);
            mPaint.setColor(Color.rgb(0x9e + colorChange, 0x9e + colorChange, 0x9e + colorChange));
            canvas.drawArc(mRect, 0, 180, false, mPaint);
        }
        if (BuildConfig.DEBUG) {
            System.out.println("relic effect took => "
                    + (System.currentTimeMillis() - time) + " milli secs");
        }

    }

    private void initializeDrawing() {
        mPaint = new Paint();
        mRect = new RectF();
        mRect.left = 0;
        mRect.right = getRight() + WIDTH_FACTOR / 2.0f;
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(STROKE);
    }


}
