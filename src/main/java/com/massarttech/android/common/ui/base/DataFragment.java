package com.massarttech.android.common.ui.base;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.massarttech.android.common.R;
import com.massarttech.android.common.databinding.FragmentDataGenericBinding;
import com.massarttech.android.common.helper.Style;
import com.massarttech.android.common.ui.widget.ErrorView;
import com.massarttech.android.common.ui.widget.RelicSearchView;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public abstract class DataFragment<T> extends BaseFragment {
    private static final String TAG = DataFragment.class.getSimpleName();

    protected RecyclerView recyclerView;
    protected ErrorView errorView;
    protected SwipeRefreshLayout refreshLayout;
    protected RelicSearchView searchView;

    private Disposable disposable;
    private FragmentDataGenericBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentDataGenericBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = binding.recyclerView;
        errorView = binding.errorView;
        refreshLayout = binding.refreshLayout;
        searchView = binding.searchView;
        Style style = new Style(context);
        refreshLayout.setColorSchemeColors(style.getPrimaryColor(), style.getPrimaryDarkColor(), style.getPrimaryColor());
        refreshLayout.setOnRefreshListener(this::loadData);
        refreshLayout.setProgressBackgroundColorSchemeColor(style.getSecondaryColor());
        errorView.setListener(this::loadData);
        searchView.setListener(this::performSearch);
        refreshLayout.setEnabled(canRefresh());
        loadData();
    }

    protected void onSuccess(@NonNull List<T> data) {
        hideProgress();
        searchView.setVisibility(hasSearch() ? View.VISIBLE : View.GONE);
        errorView.setVisibility(View.GONE);
        disposable.dispose();
        if (hasDivider() && recyclerView.getItemDecorationCount() == 0) {
            recyclerView.addItemDecoration(itemDecoration());
        }
        recyclerView.setLayoutManager(getLayoutManager());
        recyclerView.setAdapter(getAdapter(data));
        recyclerView.setVisibility(View.VISIBLE);
    }

    public RecyclerView.ItemDecoration itemDecoration() {
        return new DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
    }

    public void performSearch(@Nullable String text) {
        Log.i(TAG, "performSearch: No one is handling search");
    }

    @NonNull
    public abstract RecyclerView.Adapter getAdapter(@NonNull List<T> data);

    @NonNull
    public abstract Observable<List<T>> getCall();

    public boolean hasSearch() {
        return false;
    }

    public boolean canRefresh() {
        return true;
    }

    @DrawableRes
    public int getErrorIcon() {
        return R.drawable.ic_signal_wifi_off_black_24dp;
    }

    private void onError(@NonNull Throwable error) {
        disposable.dispose();
        hideProgress();
        presentError(error);

    }

    protected void presentError(@NonNull Throwable error) {
        hideProgress();
        recyclerView.setVisibility(View.GONE);
        errorView.getIcError().setImageResource(getErrorIcon());
        errorView.setErrorText(error.getMessage());
        errorView.setVisibility(View.VISIBLE);
    }


    private void hideProgress() {
        refreshLayout.setRefreshing(false);
    }

    private void showProgress() {
        errorView.setVisibility(View.GONE);
        searchView.setVisibility(View.GONE);
        refreshLayout.setRefreshing(true);
    }

    private void loadData() {
        showProgress();
        disposable = getCall()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onSuccess, this::onError);
    }


    @Override
    public void onStop() {
        super.onStop();
        if (disposable != null) {
            disposable.dispose();
        }
    }

    @NonNull
    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(context);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (getContext() != null && recyclerView != null && recyclerView.getAdapter() != null) {
            //if GridLayoutManager maybe need to refresh number of columns
            recyclerView.setLayoutManager(getLayoutManager());
            recyclerView.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroyView() {
        binding = null;
        super.onDestroyView();
    }

    public boolean hasDivider() {
        return false;
    }

    public void refresh() {
        loadData();
    }
}
