package com.massarttech.android.common.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Shader;
import android.text.TextPaint;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.massarttech.android.common.R;

/**
 * Same as {@link androidx.appcompat.widget.AppCompatTextView}
 * Applies gradient to text view
 * It has startColor and endColor that can be applied using layout attributes
 * <pre>
 *      <com.massarttech.android.commons.widgets.GradientTextView
 *                     android:id="@+id/btnEnglish"
 *                     android:layout_width="wrap_content"
 *                     android:layout_height="wrap_content"
 *                     android:text="@string/english"
 *                     app:grad_start_color="@color/blueSecondaryColor"
 *                     app:grad_end_color="@color/greenSecondaryColor"
 *                     android:layout_alignParentStart="true"
 *                     />
 * </pre>
 */
public class GradientTextView extends AppCompatTextView {
    private int            startColor = Color.parseColor("#3f6cb2");
    private int            endColor   = Color.parseColor("#9abf46");
    private LinearGradient linearGradient;

    public GradientTextView(Context context) {
        super(context);
        init(context, null);
    }

    public GradientTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public GradientTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.GradientTextView);
            startColor = typedArray.getColor(R.styleable.GradientTextView_grad_start_color,
                    startColor);
            endColor = typedArray.getColor(R.styleable.GradientTextView_grad_end_color,
                    endColor);
            typedArray.recycle();

        }

        TextPaint paint = getPaint();
        float     width = getWidth();
        if (getText() != null) {
            width = paint.measureText(getText().toString());

        }
        linearGradient = new LinearGradient(0, 0, width, getTextSize(),
                new int[]{startColor, endColor}, new float[]{0, 1},
                Shader.TileMode.CLAMP);

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        getPaint().setShader(linearGradient);
        setTextColor(startColor);

    }
}