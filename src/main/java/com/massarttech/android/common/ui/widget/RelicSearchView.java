package com.massarttech.android.common.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.google.android.material.card.MaterialCardView;
import com.massarttech.android.common.R;
import com.massarttech.android.common.helper.Style;

public class RelicSearchView extends LinearLayout implements TextWatcher {
    private MaterialCardView searchView;
    private ImageView        btnCloseSearch;
    private ImageView        btnBack;
    private EditText         etSearch;
    private QueryListener    listener;
    private Style            style;

    private BackPressListener backPressListener;


    public RelicSearchView(Context context) {
        super(context);
        init(context, null);
    }

    public RelicSearchView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RelicSearchView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.search_view, this, true);
        style          = new Style(context);
        searchView     = findViewById(R.id.rootView);
        btnCloseSearch = findViewById(R.id.closeIcon);
        etSearch       = findViewById(R.id.etSearch);
        btnBack        = findViewById(R.id.backIcon);
        etSearch.addTextChangedListener(this);
        btnCloseSearch.setOnClickListener(v -> {
            etSearch.setText("");
            etSearch.clearFocus();
        });
        if (attrs != null) {
            TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.RelicSearchView);
            String     text  = array.getString(R.styleable.RelicSearchView_android_hint);
            etSearch.setHint(text);
            array.recycle();
        }
        searchView.setOnClickListener(v -> performClick());

    }

    public void isBack() {
        btnCloseSearch.setVisibility(GONE);
        btnBack.setVisibility(VISIBLE);
        btnBack.setOnClickListener(v -> {
            if (backPressListener != null) {
                backPressListener.onaBackPressed();
            }
        });
    }

    public void setBackPressListener(@Nullable BackPressListener backPressListener) {
        if (backPressListener != null) {
            isBack();
        }
        this.backPressListener = backPressListener;
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        super.setOnClickListener(l);
        boolean enable = l != null;
        etSearch.setEnabled(!enable);
        View view = findViewById(R.id.overlayView);
        view.setVisibility(enable ? VISIBLE : GONE);
        view.setOnClickListener(l);
    }

    public void setListener(@Nullable QueryListener listener) {
        this.listener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String text = etSearch.getText().toString();
        toggleClose(!TextUtils.isEmpty(text));
        if (listener != null) {
            listener.onQuerySubmitted(etSearch.getText().toString());
        }
    }

    private void toggleClose(boolean enabled) {
        btnCloseSearch.setEnabled(enabled);
        btnCloseSearch.setColorFilter(enabled ? style.getPrimaryColor() : Color.GRAY);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    public interface QueryListener {
        void onQuerySubmitted(@Nullable String text);
    }

    public interface BackPressListener {
        void onaBackPressed();
    }
}
