package com.massarttech.android.common.ui.widget.button;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.massarttech.android.common.R;

public class FloatingProgressButton extends FrameLayout {
    private ProgressBar          progressBar;
    private FloatingActionButton button;
    private int                  icon = 0;
    private boolean              showingProgress;

    public FloatingProgressButton(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public FloatingProgressButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FloatingProgressButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        button.setEnabled(enabled);
    }

    private void init(@NonNull Context context,
                      @Nullable AttributeSet attributeSet) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.progress_floating_button, this, true);
        button      = view.findViewById(R.id.relicButton);
        progressBar = view.findViewById(R.id.relicButtonProgress);
        if (attributeSet != null) {
            @SuppressLint("CustomViewStyleable")
            TypedArray attrs = context
                    .obtainStyledAttributes(attributeSet, R.styleable.ProgressButton);
            button.setEnabled(attrs.
                    getBoolean(R.styleable.ProgressButton_enabled, true));
            icon = attrs.getResourceId(R.styleable.ProgressButton_icon, 0);
            if (icon != 0) {
                button.setImageResource(icon);
            }
            attrs.recycle();
        }
        button.setOnClickListener(view1 -> view.performClick());
    }

    public void show() {
        setVisibility(VISIBLE);
        button.show();
        if (showingProgress) {
            progressBar.setVisibility(VISIBLE);
        }
    }

    public void hide() {
        setVisibility(GONE);
        button.hide();
        progressBar.setVisibility(GONE);
    }

    @SuppressLint("ResourceType")
    public void showProgress() {
        showingProgress = true;
        button.setEnabled(false);
        button.setImageDrawable(null);
        progressBar.setVisibility(VISIBLE);
    }

    public void hideProgress() {
        showingProgress = false;
        if (icon != 0) {
            button.setImageResource(icon);
        }
        button.setEnabled(true);
        progressBar.setVisibility(GONE);
    }

    public FloatingActionButton getButton() {
        return button;
    }

    public void toggleProgress(boolean show) {
        if (show) {
            showProgress();
        } else {
            hideProgress();
        }
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setImageResource(@DrawableRes int icon) {
        this.icon = icon;
        if (icon != 0) {
            button.setImageResource(icon);
        }
    }
}
