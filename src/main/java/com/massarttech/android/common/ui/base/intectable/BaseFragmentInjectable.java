package com.massarttech.android.common.ui.base.intectable;

import android.app.Activity;
import android.content.Context;

import dagger.android.support.DaggerFragment;
import java8.util.Optional;


public abstract class BaseFragmentInjectable extends DaggerFragment {
    protected Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    public Optional<Activity> activity() {
        return Optional.ofNullable(getActivity());
    }

    public Optional<Context> context() {
        return Optional.ofNullable(getContext());
    }
}
