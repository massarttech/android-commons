package com.massarttech.android.common.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.google.android.material.textfield.TextInputLayout;
import com.massarttech.android.common.R;

@SuppressWarnings("unused")
public class RelicEditText extends LinearLayout {
    private EditText        editText;
    private TextInputLayout textInputLayout;
    private ImageView       imageView;

    public RelicEditText(Context context) {
        super(context);
        init(context, null);
    }

    public RelicEditText(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RelicEditText(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(@NonNull Context context, @Nullable AttributeSet attrs) {
        LayoutInflater.from(context)
                .inflate(R.layout.relic_edit_text, this, true);
        textInputLayout = findViewById(R.id.textInputLayout);
        editText = findViewById(R.id.editText);
        imageView = findViewById(R.id.imageView);
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RelicEditText);
            applyAttributes(typedArray);
            typedArray.recycle();
        }

    }

    private void applyAttributes(@NonNull TypedArray attrs) {
        boolean passwordToggle = attrs.getBoolean(R.styleable.RelicEditText_relic_edit_passwordToggleEnabled,
                false);
        int tintColor = attrs.getColor(R.styleable.RelicEditText_relic_edit_imageTint, Color.GRAY);
        int imageRes = attrs.getResourceId(R.styleable.RelicEditText_relic_edit_image,
                R.drawable.ic_warning);
        int     inputType = attrs.getInt(R.styleable.RelicEditText_relic_edit_inputType, 0x00000001);
        boolean password  = attrs.getBoolean(R.styleable.RelicEditText_relic_edit_password, false);
        int     maxLength = attrs.getInt(R.styleable.RelicEditText_relic_edit_maxLength, Integer.MAX_VALUE);
        int     maxLines  = attrs.getInt(R.styleable.RelicEditText_relic_edit_maxLines, 10);
        String  text      = attrs.getString(R.styleable.RelicEditText_relic_edit_text);
        if (text == null) {
            editText.setHint(attrs.getString(R.styleable.RelicEditText_relic_edit_hint));
        } else {
            editText.setText(text);
        }

        editText.setMaxLines(maxLines);
        editText.setFilters(new InputFilter[]{
                new InputFilter.LengthFilter(maxLength)
        });
        editText.setInputType(inputType);
        textInputLayout.setPasswordVisibilityToggleEnabled(passwordToggle);
        imageView.setImageResource(imageRes);
        imageView.setColorFilter(tintColor);
    }

    public EditText getEditText() {
        return editText;
    }

    public TextInputLayout getTextInputLayout() {
        return textInputLayout;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public CharSequence getText() {
        return editText.getText();
    }

    public void setText(@StringRes int resId) {
        editText.setText(resId);
    }

    public void setText(CharSequence text) {
        editText.setText(text);
    }

    public void setError(CharSequence text) {
        editText.setError(text);
    }

    public void setError(@StringRes int resId) {
        setError(getContext().getString(resId));
    }

    public void setImage(@DrawableRes int resId) {
        imageView.setImageResource(resId);
    }

    private void setImage(@NonNull Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    public void setImage(@NonNull Drawable drawable) {
        imageView.setImageDrawable(drawable);
    }

    public void enablePasswordToggle() {
        textInputLayout.setPasswordVisibilityToggleEnabled(true);
    }

    public void disablePasswordToggle() {
        textInputLayout.setPasswordVisibilityToggleEnabled(false);
    }

    public boolean isPasswordToggleEnabled() {
        return textInputLayout.isPasswordVisibilityToggleEnabled();
    }

    public void setHint(CharSequence hint) {
        editText.setHint(hint);
    }

    public void setHint(@StringRes int resId) {
        editText.setHint(resId);
    }

}
