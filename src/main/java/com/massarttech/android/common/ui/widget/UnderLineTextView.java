package com.massarttech.android.common.ui.widget;

import android.content.Context;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Underlined {@link android.widget.TextView}
 */
public class UnderLineTextView extends AppCompatTextView {
    private boolean modifyingText = false;

    public UnderLineTextView(Context context) {
        super(context);
        init();
    }

    public UnderLineTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UnderLineTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (modifyingText)
                    return;
                underlineText();
            }
        });

        underlineText();
    }

    private void underlineText() {
        if (modifyingText)
            return;

        modifyingText = true;

        SpannableString content = new SpannableString(getText());
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        setText(content);

        modifyingText = false;
    }
}