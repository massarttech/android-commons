package com.massarttech.android.common.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.massarttech.android.common.R;

import java8.lang.FunctionalInterface;

public class RelicLoadingSpinner extends RelativeLayout {
    private Spinner     spinner;
    private ProgressBar progressBar;
    private ImageView   btnReload;
    private View        loadingFrame;
    private TextView    helperTextView;

    private ReloadListener reloadListener;

    public RelicLoadingSpinner(Context context) {
        super(context);
        init(context, null);
    }

    public RelicLoadingSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RelicLoadingSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);

    }

    public void setReloadListener(ReloadListener reloadListener) {
        this.reloadListener = reloadListener;
    }

    private void init(@NonNull Context context, AttributeSet attrs) {
        LayoutInflater.from(context)
                .inflate(R.layout.relic_loading_spinner, this, true);
        spinner = findViewById(R.id.spinner);
        progressBar = findViewById(R.id.pbSpModel);
        btnReload = findViewById(R.id.btnReloadModel);
        loadingFrame = findViewById(R.id.modelFrame);
        helperTextView = findViewById(R.id.tvHelperText);
        btnReload.setOnClickListener(v -> {
            if (reloadListener != null) {
                reloadListener.onReload();
            }
        });
        if (attrs != null) {
            applyAttrs(attrs);
        }
    }

    private void applyAttrs(@NonNull AttributeSet attributeSet) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet, R.styleable.RelicLoadingSpinner);
        String     helperText = typedArray.getString(R.styleable.RelicLoadingSpinner_helperText);
        if (helperText != null) {
            helperTextView.setVisibility(VISIBLE);
            helperTextView.setText(Html.fromHtml(helperText));
        }
        typedArray.recycle();
    }

    public TextView getHelperTextView() {
        return helperTextView;
    }

    public void showLoading() {
        btnReload.setVisibility(GONE);
        loadingFrame.setVisibility(VISIBLE);
        progressBar.setVisibility(VISIBLE);
    }

    public void hideLoading() {
        progressBar.setVisibility(GONE);
        loadingFrame.setVisibility(GONE);
    }

    public void showReloadButton() {
        loadingFrame.setVisibility(VISIBLE);
        btnReload.setVisibility(VISIBLE);
    }

    @NonNull
    public Spinner getSpinner() {
        return spinner;
    }

    @FunctionalInterface
    public interface ReloadListener {
        void onReload();
    }
}
