package com.massarttech.android.common.keyboard;

public interface KeyboardVisibilityEventListener {

    void onVisibilityChanged(boolean isOpen);
}
