package com.massarttech.android.common.keyboard;

import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver;

import java.lang.ref.WeakReference;

public class SimpleUnregistrar implements Unregistrar {

    private final WeakReference<Activity> mActivityWeakReference;

    private final WeakReference<ViewTreeObserver.OnGlobalLayoutListener> mOnGlobalLayoutListenerWeakReference;

    SimpleUnregistrar(Activity activity, ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener) {
        mActivityWeakReference = new WeakReference<>(activity);
        mOnGlobalLayoutListenerWeakReference = new WeakReference<>(globalLayoutListener);
    }

    @Override
    public void unregister() {
        Activity                                activity             = mActivityWeakReference.get();
        ViewTreeObserver.OnGlobalLayoutListener globalLayoutListener = mOnGlobalLayoutListenerWeakReference.get();

        if (null != activity && null != globalLayoutListener) {
            View activityRoot = KeyboardVisibilityEvent.getActivityRoot(activity);
            activityRoot.getViewTreeObserver()
                    .removeOnGlobalLayoutListener(globalLayoutListener);
        }

        mActivityWeakReference.clear();
        mOnGlobalLayoutListenerWeakReference.clear();
    }

}
