package com.massarttech.android.common.backend.task;

@FunctionalInterface
public interface OnComplete {
    void onCompleted();
}
