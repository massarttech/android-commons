package com.massarttech.android.common.backend.task;

@FunctionalInterface
public interface OnPreExecute {
    void onPreExecute();
}
