package com.massarttech.android.common.backend;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class Callback<T> implements retrofit2.Callback<T>, LifecycleObserver {
    @Nullable
    private OnApiResponse<T> apiResponse;
    private boolean          attached = true;
    @Nullable
    private Call<T>          call;
    @Nullable
    private Throwable        error;
    @Nullable
    private Response<T>      response;
    private OnDone           onDone;

    public Callback(@Nullable OnApiResponse<T> apiResponse) {
        this.apiResponse = apiResponse;
    }

    public void setOnDone(@NonNull OnDone onDone) {
        this.onDone = onDone;
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
        if (!attached) {
            this.call = call;
            this.response = response;
            return;
        }
        this.call = null;
        this.response = null;
        if (onDone != null) {
            onDone.callDone();
        }
        if (this.apiResponse != null) {
            apiResponse.onResponse();
            T body = response.body();
            if (body != null) {
                apiResponse.onSuccess(body);
            } else {
                parseErrorBody(response.errorBody());
            }
        }
    }

    private void parseErrorBody(@Nullable ResponseBody errorBody) {
        if (apiResponse != null && attached) {
            if (errorBody != null) {
                try {
                    apiResponse.onError(errorBody.string());
                } catch (IOException e) {
                    apiResponse.onError(e.getLocalizedMessage());
                }
            } else {
                apiResponse.onError(null);
            }
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        if (!attached) {
            this.call = call;
            this.error = t;
            return;
        }
        this.call = null;
        this.error = null;
        if (onDone != null) {
            onDone.callDone();
        }
        if (apiResponse == null) {
            return;
        }
        apiResponse.onResponse();
        apiResponse.onError(t.getLocalizedMessage());
    }


    @OnLifecycleEvent(value = Lifecycle.Event.ON_DESTROY)
    public void onComponentDestroy() {
        apiResponse = null;
    }


    @OnLifecycleEvent(value = Lifecycle.Event.ON_RESUME)
    public void onComponentResumed() {
        attached = true;
        if (call != null) {
            if (error != null) {
                onFailure(call, error);
            } else if (response != null) {
                onResponse(call, response);

            }
        }
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_PAUSE)
    public void onComponentPaused() {
        attached = false;
    }

    @FunctionalInterface
    public interface OnDone {
        void callDone();
    }

}