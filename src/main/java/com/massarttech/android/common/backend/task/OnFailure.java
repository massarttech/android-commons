package com.massarttech.android.common.backend.task;


import androidx.annotation.NonNull;

import com.massarttech.android.common.domain.ApiError;

@FunctionalInterface
public interface OnFailure {
    void onFailure(@NonNull ApiError error);
}
