package com.massarttech.android.common.backend;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by bullhead on 3/15/18.
 */
@FunctionalInterface
public interface OnApiResponse<T> {
    void onSuccess(@NonNull T t);

    default void onResponse() {

    }

    default void onError(@Nullable String message) {
        System.out.println(message);
    }
}
