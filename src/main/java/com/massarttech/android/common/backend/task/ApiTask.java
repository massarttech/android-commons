package com.massarttech.android.common.backend.task;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;

import com.google.gson.Gson;
import com.massarttech.android.common.backend.Callback;
import com.massarttech.android.common.backend.OnApiResponse;
import com.massarttech.android.common.domain.ApiError;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Example Usage:
 * <code>
 * UserApi.getInstance()
 * .verifyUser(phone, token)
 * .observe(this)
 * .success(this::onSuccess)
 * .failure(this::onError)
 * .before(this::showProgress)
 * .completed(this::hideProgress)
 * .executeAsync();
 * </code>
 *
 * @param <Result> Result that will be response of the network call
 */
@SuppressWarnings("unused")
public final class ApiTask<Result> implements OnApiResponse<Result> {
    private static final String TAG = ApiTask.class.getSimpleName();

    private final List<OnSuccess<Result>> successSubscribers;
    private final List<OnFailure>         failureSubscriber;
    private final List<OnComplete>        completeSubscribers;
    private final List<OnPreExecute>      preExecuteSubscribers;
    private final Call<Result>            call;
    private       Lifecycle               lifecycle;

    public ApiTask(@NonNull Call<Result> call) {
        this.call             = call;
        successSubscribers    = Collections.synchronizedList(new ArrayList<>());
        failureSubscriber     = Collections.synchronizedList(new ArrayList<>());
        completeSubscribers   = Collections.synchronizedList(new ArrayList<>());
        preExecuteSubscribers = Collections.synchronizedList(new ArrayList<>());
    }

    public synchronized void cancel() {
        call.cancel();
    }

    public synchronized void executeAsync() {
        final Callback<Result> callback = new Callback<>(this);
        callback.setOnDone(() -> {
            if (lifecycle != null) {
                lifecycle.removeObserver(callback);
            }
        });
        for (OnPreExecute before : preExecuteSubscribers) {
            before.onPreExecute();
        }
        if (lifecycle != null) {
            lifecycle.addObserver(callback);
        }
        call.enqueue(callback);
    }

    public synchronized <LF extends LifecycleOwner> ApiTask<Result> observe(@NonNull LF lifecycle) {
        this.lifecycle = lifecycle.getLifecycle();
        return this;
    }

    public synchronized ApiTask<Result> observe(@NonNull Lifecycle lifecycle) {
        this.lifecycle = lifecycle;
        return this;
    }

    public synchronized Response<Result> execute() throws IOException {
        return call.execute();
    }

    @NonNull
    public synchronized ApiTask<Result> success(@NonNull OnSuccess<Result> success) {
        successSubscribers.add(success);
        return this;
    }

    @NonNull
    public synchronized ApiTask<Result> before(@NonNull OnPreExecute before) {
        this.preExecuteSubscribers.add(before);
        return this;
    }

    @NonNull
    public synchronized ApiTask<Result> completed(@NonNull OnComplete complete) {
        this.completeSubscribers.add(complete);
        return this;
    }

    @NonNull
    public synchronized ApiTask<Result> failure(@NonNull OnFailure failure) {
        this.failureSubscriber.add(failure);
        return this;
    }


    public void onSuccess(@NonNull Result result) {
        for (OnSuccess<Result> subscriber : successSubscribers) {
            subscriber.onSuccess(result);
        }
    }

    @Override
    public void onResponse() {
        for (OnComplete complete : completeSubscribers) {
            complete.onCompleted();
        }
    }

    @Override
    public void onError(@Nullable String message) {
        ApiError error = makeError(message);
        for (OnFailure failure : failureSubscriber) {
            failure.onFailure(error);
        }
    }

    @NonNull
    private ApiError makeError(@Nullable String message) {
        try {
            ApiError error = new Gson().fromJson(message, ApiError.class);
            if (error == null) {
                message = "unknown";
                throw new NullPointerException();
            }
            return error;
        } catch (Exception e) {
            Log.e(TAG, "makeError: error in handling error because " + e.getLocalizedMessage());
            return ApiError.builder()
                    .message(message)
                    .resolvable(false)
                    .timestamp(System.currentTimeMillis())
                    .build();
        }
    }
}
