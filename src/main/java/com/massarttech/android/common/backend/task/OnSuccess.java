package com.massarttech.android.common.backend.task;

import androidx.annotation.NonNull;

public interface OnSuccess<Result> {
    void onSuccess(@NonNull Result result);
}
