package com.massarttech.android.common.backend;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;

public class LifecycleAwareCallback<T> {
    private final Callback<T> callback;
    private       Lifecycle   lifecycle;

    public LifecycleAwareCallback(@Nullable Callback<T> callback) {
        this.callback = callback;
        if (callback != null) {
            callback.setOnDone(() -> {
                if (lifecycle != null) {
                    lifecycle.removeObserver(callback);
                }
            });
        }
    }

    public void addObserver(@NonNull Lifecycle lifecycle) {
        if (this.callback == null) {
            return;
        }
        this.lifecycle = lifecycle;
        lifecycle.addObserver(callback);
    }
}
